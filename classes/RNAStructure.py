from anytree import RenderTree
from .SSE import SSENode
from ..utils.structure import bracket_to_index


class SSETree:

    def __init__(self):
        self.root = None

    @classmethod
    def from_bracket(cls, structure, seq=""):
        tree = cls()
        tree.structure = structure
        tree.seq = seq
        index = bracket_to_index(structure)
        seq = "#"+seq+"#"
        def aux(ind, parent=None):
            tmp = []
            k = ind+1
            j = ind
            res = []
            w = []
            while True:
                if index[k] == -1:
                    k += 1
                elif index[k] > k:
                    tmp.append((k,index[k]))
                    if seq:
                        w.append(seq[j:k+1])
                    res.append(aux(k))
                    j = index[k]
                    k = index[k] + 1
                else:
                    if seq:
                        w.append(seq[j:k+1])
                    break
            node = SSENode([(ind,index[ind])]+tmp, root=(not ind), seq="&".join(w))
            for s in res:
                s.parent = node
            return node
        tree.root = aux(0)
        return tree

    def print_tree(self):
        """
        A simple tree printer
        """
        for pre,_,node in RenderTree(self.root):
            treestr = u"%s%s" % (pre,node.label)
            print(treestr.ljust(0),node.get_strands_len())

    def get_sequence(self):
        if tree.seq:
            return tree.seq
        else:
            raise Exception("No assigned sequence")

if __name__ == "__main__":
    s = "(((.((...))((...)))))"
    tree = SSETree.from_bracket(s)
    tree.print_tree()
